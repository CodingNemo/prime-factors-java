package kata;

import java.util.ArrayList;
import java.util.List;

import static java.lang.Math.floor;
import static java.lang.Math.sqrt;
import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;

class PrimeFactors {
    static List<Integer> decompose(int source) {
        if(asList(0,1).contains(source)) {
            return singletonList(source);
        }

        return decomposeRecursion(source);
    }

    private static List<Integer> decomposeRecursion(int number) {
        for(int divisor = 2; divisor <= floor(number / sqrt(2)); divisor++){
            if (isDivisibleBy(number, divisor)) {
                int remainder = number / divisor;

                ArrayList<Integer> decomposition = new ArrayList<>();
                decomposition.add(divisor);
                decomposition.addAll(decomposeRecursion(remainder));

                return decomposition;
            }
        }
        return singletonList(number);
    }

    private static boolean isDivisibleBy(int source, int number) {
        return source % number == 0;
    }
}
