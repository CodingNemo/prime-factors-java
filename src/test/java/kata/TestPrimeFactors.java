package kata;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Arrays;
import java.util.List;

import static java.util.Arrays.asList;
import static junitparams.JUnitParamsRunner.$;
import static org.assertj.core.api.Assertions.assertThat;

@RunWith(JUnitParamsRunner.class)
public class TestPrimeFactors {
    @Test
    @Parameters({"0", "1"})
    public void TestPrimeFactorDecompositionOfSpecialCases(int number) {
        List<Integer> factors = PrimeFactors.decompose(number);
        assertThat(factors).containsExactly(number);
    }

    @Test
    @Parameters({"2", "3", "5", "7"})
    public void TestPrimeFactorDecompositionOfPrimeNumberShouldBeTheNumber(int number) {
        List<Integer> factors = PrimeFactors.decompose(number);

        assertThat(factors).containsExactly(number);
    }

    private Object notPrimeNumbers() {
        return $(
                $(4, asList(2, 2)),
                $(6, asList(2, 3)),
                $(10, asList(2, 5)),
                $(9, asList(3, 3)),
                $(15, asList(3, 5)),
                $(35, asList(5, 7)),
                $(8, asList(2, 2, 2)),
                $(66, asList(2, 3, 11)),
                $(187, asList(11, 17))
                );
    }

    @Test
    @Parameters(method = "notPrimeNumbers")
    public void TestPrimeFactorDecompositionShouldBePrimeNumbers(
            int number, List<Integer> expected) {
        List<Integer> factors = PrimeFactors.decompose(number);
        assertThat(factors).isEqualTo(expected);
    }
}
